<?php namespace Larastart\Handlers\Events\Build;


use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Larastart\Events\Build\ComposerUpdated;
use Larastart\Services\Config;


class UpdateProvidersAliasesConfig
{
    protected $config;
    protected $project;

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Handle the event.
     *
     * @param  LarastartEventsBuildComposerUpdating $event
     * @return void
     */
    public function handle(ComposerUpdated $event)
    {
        $this->project = $event->getProject();
        $this->config->setConfigBasePath($this->project->configPath);
        foreach ($this->project->packages as $package) {
            $this->updateProviders($package->providers);
            $this->updateAliases($package->aliases);
        }
    }

    public function updateProviders($providers)
    {
        if (!empty($providers))
        {
            $this->config->update('app.providers', $providers);
		}
    }

    public function updateAliases($aliases)
    {
        if (!empty($aliases)) {
            $aliases = array_pluck($aliases, 'facade', 'alias');
            $this->config->update('app.aliases', $aliases);
        }
    }


}
