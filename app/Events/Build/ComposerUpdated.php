<?php namespace Larastart\Events\Build;


use Illuminate\Queue\SerializesModels;

class ComposerUpdated extends BuildEvent {

	use SerializesModels;

}
