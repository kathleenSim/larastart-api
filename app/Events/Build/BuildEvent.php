<?php namespace Larastart\Events\Build;

use Larastart\Events\Event;

use Illuminate\Queue\SerializesModels;

class BuildEvent extends Event {

	use SerializesModels;

	protected $project;



	public function __construct($project)
	{
		$this->project = $project;
	}

	function getProject()
	{
		return $this->project;
	}

	function setProject($project)
	{
		$this->project = $project;
	}



}
