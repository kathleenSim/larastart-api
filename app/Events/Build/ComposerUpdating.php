<?php namespace Larastart\Events\Build;

use Larastart\Events\Event;

use Illuminate\Queue\SerializesModels;

class ComposerUpdating extends BuildEvent {

	use SerializesModels;

	protected $composer;

	protected $composerPath;

	function getComposerContent()
	{
		if(is_null($this->composer))
		{
			$this->composer = json_decode(\File::get($this->getComposerFilePath()),true);
		}
		return $this->composer;
	}

	function setComposerContent($composer)
	{
		$this->composer = $composer;
		\File::put($this->composerPath,json_encode($this->composer,JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
	}


	function getComposerFilePath()
	{
		if(is_null($this->composerPath))
		{
			$this->composerPath = $this->project->basePath.'/composer.json';
		}
		return $this->composerPath;
	}

	function setComposerFilePath($path)
	{
		$this->composerPath = $path;
	}



}
