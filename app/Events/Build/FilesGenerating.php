<?php namespace Larastart\Events\Build;

use Larastart\Events\Event;

use Illuminate\Queue\SerializesModels;

class FilesGenerating extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

}
