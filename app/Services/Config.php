<?php  namespace Larastart\Services;

use Illuminate\Support\Facades\File;

/**
 * @property  basePath
 */
class Config
{

    protected $filesys;
    protected $configPath;
    protected $basePath;
    protected $updates;
    protected $contentsCache;

    function __construct()
    {
        $this->filesys = \App::make('files');

    }

    function setConfigBasePath($path)
    {
        $this->basePath = $path;
    }

    function get($path)
    {

    }

    function update($key, $content)
    {
        $this->key = $key;
        $this->updates = $content;
        $this->getConfigContents();
        $config = $this->getConfig();

        if (str_contains($this->config_key, '.')) {
            $key = substr($this->config_key, 0, strpos($this->config_key, '.'));
            $to_update = substr($this->config_key, strpos($this->config_key, '.') + 1);
            $content = array_get($config, $key);
            array_set($content, $to_update, $this->updates);
            $this->config_key = $key;
            $this->replaceConfig($content);
        } else {
            $content = array_get($config, $this->config_key);
            if (is_array($content)) {
                $updated = array_merge($content, $this->updates);
                $updated = array_unique($updated);
            } else {
                $updated = $this->updates;
            }
            $this->replaceConfig($updated);
        }
        $this->putConfigContents();
    }

    public function getConfigContents()
    {
        if (!isset($this->contentsCache)) {
            $this->contentsCache = $this->filesys->get($this->getConfigPath());
        }

        return $this->contentsCache;
    }


    public function getConfig()
    {
        $config = require($this->getConfigPath());
        return $config;
    }

    protected function getConfigPath()
    {
        if (is_null($this->configPath)) {
            if (is_null($this->basePath))
                \App::abort(500, 'Config base path not specified');

            $key = explode('.', $this->key);
            $file = [];
            $path = '';
            do {
                if (empty($key)) {
                    \App::abort(404, 'Config File not found. Path: ' . $path);
                }
                $file[] = array_shift($key);
                $path = $this->basePath . '/' . implode('/', $file) . '.php';
            } while (!$this->filesys->exists($path));
            $this->configPath = $path;
            $this->config_key = implode('.', $key);
        }

        return $this->configPath;
    }

    protected function putConfigContents()
    {
        file_put_contents($this->getConfigPath(),$this->contentsCache);
    }

    protected function replaceConfig($replacement)
    {
        $method = 'replace' . studly_case(gettype($replacement)) . 'Config';
        if (method_exists($this, $method))
            return $this->$method($replacement);
        $this->replaceStringConfig($replacement);
    }

    protected function replaceStringConfig($replacement)
    {
        $pattern = "/'$this->config_key' => '(\\S)*'/m";
        $replacement = "'$this->config_key' => '$replacement'";
        $this->contentsCache = preg_replace($pattern, $replacement, $this->contentsCache);
    }

    protected function replaceBooleanConfig($replacement)
    {
        $replacement = $replacement ? 'true' : 'false';
        $replacement = "'$this->config_key' => $replacement";
        $pattern = "/'$this->config_key' => (\\w)*/m";
        $this->contentsCache = preg_replace($pattern, $replacement, $this->contentsCache);
    }

    protected function replaceArrayConfig($replacement)
    {
        if (isset($replacement[0])) {
            $header = "'{$this->config_key}' => array(\n\n\t\t'";
            $values = implode("',\n\t\t'", $replacement);
            $content = $header . $values . "',\n\n\t)";
        } else {
            $header = "'{$this->config_key}' => ";
            $values = var_export($replacement, true);
            $content = $header . $values;
            $content = str_replace('(', "(\n\t", $content);
            $content = str_replace(')', "\n\t)", $content);
            $content = str_replace("\n  ", "\n\t\t", $content);
            $content = str_replace("array (", "array(", $content);
        }
        $subarrays = substr_count($content, 'array');
        $pattern_long = "/'{$this->config_key}' => array\(";
        do {
            $pattern_long .= "[^)]*\)";
            $subarrays--;
        } while ($subarrays > 0);
        $pattern_long .= "/s";

        $pattern_short = "/'{$this->config_key}' => \[";
        do {
            $pattern_short .= "[^]]*\]";
            $subarrays--;
        } while ($subarrays > 0);
        $pattern_short .= "/s";


        $this->contentsCache = preg_replace($pattern_short, $content, $this->contentsCache);
    }


}