<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 25/02/2015
 * Time: 15:36
 */

namespace Larastart\Domain\Contracts;


interface Contract {

    public function getAll();

    public function getPaginated();

    public function getByKey($id);

    public function create($input);

    public function update($id, $input);

    public function archive($id);

    public function restore($id);

    public function delete($id);

} 