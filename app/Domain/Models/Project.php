<?php namespace Larastart\Domain\Models;
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 16/02/2015
 * Time: 20:28
 */
use Illuminate\Support\Str;
use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;


class Project extends Eloquent {

    use SoftDeletes;

    protected $fillable = array( 'owner_id', 'name', 'version', 'type', 'status', 'slug');

    protected $dates = ['deleted_at'];

    public function packages()
    {
        return $this->embedsMany('Larastart\Domain\Models\Package');
    }

    public function modules()
    {
        return $this->embedsMany('Larastart\Domain\Models\Module');
    }

    function getSlugAttribute($value)
    {
        if(is_null($value))
            $value = Str::slug($this->name);
        return $value;
    }

    public function getBuildPathAttribute()
    {
        return storage_path().'/builds/'.$this->slug;
    }

    public function getBasePathAttribute()
    {
        return $this->build_path.'/current';
    }

    public function getConfigPathAttribute()
    {
        $configPath = floatval($this->version) < 5 ? '/app/config' : '/config';
        return $this->base_path.$configPath;
    }

} 