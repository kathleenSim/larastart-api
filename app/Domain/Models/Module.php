<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 27/02/2015
 * Time: 14:05
 */

namespace Larastart\Domain\Models;

use Jenssegers\Mongodb\Model as Eloquent;


class Module extends Eloquent{

    protected $fillable = array('name');

    public function entities()
    {
        return $this->embedsMany('Larastart\Domain\Models\Entity');
    }

    public function requests()
    {
        return $this->embedsMany('Larastart\Domain\Models\Request');
    }
} 