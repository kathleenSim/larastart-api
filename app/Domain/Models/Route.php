<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 05:38
 */

namespace app\Domain\Models;

use Jenssegers\Mongodb\Model as Eloquent;


class Route extends Eloquent{

    protected $fillable = array('name');

} 