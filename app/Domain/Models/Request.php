<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 28/02/2015
 * Time: 07:47
 */

namespace Larastart\Domain\Models;

use Jenssegers\Mongodb\Model as Eloquent;


class Request extends Eloquent{

    protected $fillable = array('name');

    public function controllers()
    {
        return $this->embedsMany('App\Domain\Models\Controller');
    }

    public function views()
    {
        return $this->embedsMany('App\Domain\Models\View');
    }

    public function transformers()
    {
        return $this->embedsMany('App\Domain\Models\Transformer');
    }

    public function routes()
    {
        return $this->embedsMany('App\Domain\Models\Route');
    }
} 