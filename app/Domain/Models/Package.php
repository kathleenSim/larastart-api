<?php namespace Larastart\Domain\Models;
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 16/02/2015
 * Time: 20:28
 */

use Jenssegers\Mongodb\Model as Eloquent;


class Package extends Eloquent {


    protected $fillable = array('name');

    function isValid()
    {
        return true;
    }


} 