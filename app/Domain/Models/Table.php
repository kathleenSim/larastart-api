<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 02/03/2015
 * Time: 18:53
 */

namespace Larastart\Domain\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Table extends Eloquent{

    protected $fillable = array('name');

} 