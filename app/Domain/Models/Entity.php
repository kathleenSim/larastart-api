<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 28/02/2015
 * Time: 07:47
 */

namespace Larastart\Domain\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Entity extends Eloquent{

    protected $fillable = array('name');

    public function seeds()
    {
        return $this->embedsMany('Larastart\Domain\Models\Seed');
    }

    public function tables()
    {
        return $this->embedsMany('Larastart\Domain\Models\Table');
    }

    public function models()
    {
        return $this->embedsMany('Larastart\Domain\Models\Model');
    }
}