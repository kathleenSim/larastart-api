<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 02/03/2015
 * Time: 18:54
 */

namespace Larastart\Domain\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Seed extends Eloquent{

    protected $fillable = array('name');

} 