<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 03:07
 */

namespace App\Domain\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Controller extends Eloquent{

    protected $fillable = array('name');

} 