<?php namespace Larastart\Domain\Repositories\Eloquent;


use Illuminate\Support\Str;
use Larastart\Domain\Models\Project;
use \Larastart\Domain\Contracts\ProjectsContract;
use Validator;

/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 17/02/2015
 * Time: 04:13
 */
class ProjectsRepository implements ProjectsContract
{


    public function getAll()
    {
        return Project::all();
    }

    public function getPaginated()
    {
        return Project::paginate(5);
    }

    public function getByKey($id)

    {
        return Project::findOrFail($id);
    }

    public function create($request)
    {
        $default = [
            'version' => '5.0'
        ];
        $data = array_replace_recursive($default,$request->only(['name','version','slug']));
        if(     is_null($data['slug']))
        {
            do{
                $data['slug'] = is_null($data['slug'])? Str::slug($data['name'],'.') : Str::slug($data['name'],'.'). rand(1,99999);
                $validator = \Validator::make(['slug'=>$data['slug']],['slug'=>'unique:projects']);
            }
            while($validator->fails());
        }
        //return $project;

        //dd($data);

        $project = Project::create($data);
        $project->version = "5.0";
        $project->type = "API";
        $project->status = "new";
        $project->deleted_at = null;


        $project->save();

        return $project;

    }

    public function update($id, $input)
    {
        $project = Project::findOrFail($id);

        foreach ($input as $attr => $value) {

            $project->$attr = $value;
        }

        $project->save();

        return $project;
    }

    public function archive($id)
    {
        Project::findOrFail($id);

        Project::destroy($id);

        return response("Archived", 200);
    }

    public function restore($id)
    {
        $project = Project::withTrashed()->findOrFail($id);

        $project->restore();

        return $project;
    }


    public function delete($id)
    {
        $project = Project::withTrashed()->findOrFail($id);

        $project->forceDelete();

        return response(204);
    }


} 