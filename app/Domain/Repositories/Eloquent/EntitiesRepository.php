<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 28/02/2015
 * Time: 08:11
 */

namespace Larastart\Domain\Repositories\Eloquent;


use Larastart\Domain\Contracts\EntitiesContract;
use Larastart\Domain\Models\Project;

class EntitiesRepository implements EntitiesContract{

    public function getAll($id = null){
        $mid = func_get_arg(1);

        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->get()->toArray();

    }

    public function getPaginated(){

    }

    public function getByKey($id){

        $mid = func_get_arg(1);

        $eid = func_get_arg(2);

        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->get()->toArray();
    }

    public function create($id){

        $input = func_get_arg(1);

        $mid = func_get_arg(2);

        $entity = Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->create($input);

        $entity->deleted_at = null;

        $entity->save();

        return $entity;

    }

    public function update($id, $input){

        $mid = func_get_arg(2);

        $eid = func_get_arg(3);
        $entity = Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->find($eid);

        foreach ($input as $attr => $value) {

            $entity->$attr = $value;
        }

        $entity->save();

        return $entity;
    }

    public function archive($id){

    }

    public function restore($id){
        
    }

    public function delete($id){
        $mid = func_get_arg(1);
        $eid = func_get_args(2);

        $module = Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first;
        $module->delete();

        return response("Permanently deleted", 204);
    }

} 