<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 02/03/2015
 * Time: 19:00
 */

namespace Larastart\Domain\Repositories\Eloquent;


use Larastart\Domain\Contracts\TablesContract;
use Larastart\Domain\Models\Project;

class TablesRepository implements TablesContract{

    public function getAll($id=null)
    {
        $mid = func_get_arg(1);

        $eid = func_get_arg(2);

        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->tables()->get()->toArray();

    }

    public function getPaginated(){

    }

    public function getByKey($id){
        $mid = func_get_arg(1);

        $eid = func_get_arg(2);

        $tid = func_get_arg(3);

        return  Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->tables()->where('id', $tid)->first()->toArray();
    }

    public function create($id){
        $input = func_get_arg(1);

        $mid = func_get_arg(2);

        $eid = func_get_arg(3);

        $table = Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->tables()->create($input);

        $table->deleted_at = null;

        $table->save();

        return $table;
    }

    public function update($id, $input){

        $mid = func_get_arg(2);

        $eid = func_get_arg(3);

        $tid = func_get_arg(4);

        $table = Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->tables()->where('id', $tid)->first();

        foreach ($input as $attr => $value) {

            $table->$attr = $value;
        }

        $table->save();

        return $table;

    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){

    }

} 