<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 03:13
 */

namespace App\Domain\Repositories\Eloquent;


use App\Domain\Contracts\TransformersContract;
use App\Domain\Models\Project;

class TransformersRepository implements TransformersContract{

    public function getAll($id=null){
        $mid = func_get_arg(1);

        $rid = func_get_arg(2);

        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->transformers()->get()->toArray();


    }

    public function getPaginated(){

    }

    public function getByKey($id){

        $mid = func_get_arg(1);

        $rid = func_get_arg(2);

        $tid = func_get_arg(3);

        return  Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->transformers()->where('id', $tid)->first()->toArray();


    }

    public function create($id){

        $input = func_get_arg(1);

        $mid = func_get_arg(2);

        $rid = func_get_arg(3);

        $transformer = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->transformers()->create($input);

        $transformer->deleted_at = null;

        $transformer->save();

        return $transformer;


    }

    public function update($id, $input){

        $mid = func_get_arg(2);

        $rid = func_get_arg(3);

        $tid = func_get_arg(4);

        $transformer = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->transformers()->where('id', $tid)->first();

        foreach ($input as $attr => $value) {

            $transformer->$attr = $value;
        }

        $transformer->save();

        return $transformer;


    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){

    }

} 