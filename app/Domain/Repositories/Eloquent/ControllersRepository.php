<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 03:13
 */

namespace App\Domain\Repositories\Eloquent;

use App\Domain\Contracts\ControllersContract;
use App\Domain\Models\Project;

class ControllersRepository implements ControllersContract{

    public function getAll($id=null){
        $mid = func_get_arg(1);

        $rid = func_get_arg(2);

        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->controllers()->get()->toArray();

    }

    public function getPaginated(){

    }

    public function getByKey($id){
        $mid = func_get_arg(1);

        $rid = func_get_arg(2);

        $cid = func_get_arg(3);

        return  Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->controllers()->where('id', $cid)->first()->toArray();

    }

    public function create($id){
        $input = func_get_arg(1);

        $mid = func_get_arg(2);

        $rid = func_get_arg(3);

        $controller = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->controllers()->create($input);

        $controller->deleted_at = null;

        $controller->save();

        return $controller;

    }

    public function update($id, $input){

        $mid = func_get_arg(2);

        $rid = func_get_arg(3);

        $cid = func_get_arg(4);

        $controller = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->controllers()->where('id', $cid)->first();

        foreach ($input as $attr => $value) {

            $controller->$attr = $value;
        }

        $controller->save();

        return $controller;


    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){

    }


} 