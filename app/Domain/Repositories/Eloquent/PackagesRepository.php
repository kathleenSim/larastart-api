<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 25/02/2015
 * Time: 15:38
 */

namespace Larastart\Domain\Repositories\Eloquent;


use Larastart\Domain\Contracts\PackagesContract;
use Larastart\Domain\Models\Project;
use Packagist\Api\Client;

class PackagesRepository implements PackagesContract{

    public function getAll($id=null){

       return Project::findOrFail($id)->packages;

    }

    public function getPaginated(){

    }

    public function getByKey($id){
        $pid = func_get_arg(1);

        return Project::findOrFail($id)->packages()->where('id', $pid)->first();

    }

    public function create($id){

        $input = func_get_arg(1);

        $package = Project::findOrFail($id)->packages()->create($input);

        $package->_id;
        $package->deleted_at = null;
        $package->version = null;
        $package->options = null;
        $package->providers = null;
        $package->aliases = null;


        $package->save();

        //dd($package['name']);
        $client = new Client();
        $packageCheck = $client->get($package['name']);

        $package->name = $packageCheck->getName();

        $package->save();

//        printf(
//            'Package %s. %s.',
//            $packageCheck->getName(),
//            $packageCheck->getDescription()
//        );
        return $package;

    }

    public function update($id, $input){
        $pid = func_get_arg(2);

        $package = Project::findOrFail($id)->packages()->find($pid);

        foreach ($input as $attr => $value) {

            $package->$attr = $value;
        }

        $package->save();

        return $package;

    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){
        $pid = func_get_arg(1);

        $package = Project::findOrFail($id)->packages()->where('id', $pid)->first();
        //dd($package);
        $package->delete();

        return response("Permanently deleted", 204);
    }

} 