<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 27/02/2015
 * Time: 15:38
 */

namespace Larastart\Domain\Repositories\Eloquent;

use Larastart\Domain\Contracts\ModulesContract;
use Larastart\Domain\Models\Project;

class ModulesRepository implements ModulesContract{

    public function getAll($id=null){

        return Project::findOrFail($id)->modules;

    }

    public function getPaginated(){

    }

    public function getByKey($id){

        $mid = func_get_arg(1);

        return Project::findOrFail($id)->modules()->where('id', $mid);

    }

    public function create($id){

        $input = func_get_arg(1);

        $module = Project::findOrFail($id)->modules()->create($input);

        $module->deleted_at = null;

        $module->save();

        return $module;

    }

    public function update($id, $input){

        $mid = func_get_arg(2);

        $module = Project::findOrFail($id)->modules()->find($mid);

        foreach ($input as $attr => $value) {

            $module->$attr = $value;
        }

        $module->save();

        return $module;

    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){
        $mid = func_get_arg(1);

        $module = Project::findOrFail($id)->modules()->where('id', $mid)->first();
        $module->delete();

        return response("Permanently deleted", 204);
    }

} 