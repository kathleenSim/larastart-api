<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 03:13
 */

namespace App\Domain\Repositories\Eloquent;

use App\Domain\Contracts\ViewsContract;
use App\Domain\Models\Project;

class ViewsRepository implements ViewsContract{

    public function getAll($id=null){

        $mid = func_get_arg(1);

        $rid = func_get_arg(2);

        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->views()->get()->toArray();


    }

    public function getPaginated(){

    }

    public function getByKey($id){

        $mid = func_get_arg(1);

        $rid = func_get_arg(2);

        $vid = func_get_arg(3);

        return  Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->views()->where('id', $vid)->first()->toArray();


    }

    public function create($id){

        $input = func_get_arg(1);

        $mid = func_get_arg(2);

        $rid = func_get_arg(3);

        $view = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->views()->create($input);

        $view->deleted_at = null;

        $view->save();

        return $view;
    }

    public function update($id, $input){

        $mid = func_get_arg(2);

        $rid = func_get_arg(3);

        $vid = func_get_arg(4);

        $view = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->views()->where('id', $vid)->first();

        foreach ($input as $attr => $value) {

            $view->$attr = $value;
        }

        $view->save();

        return $view;


    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){

    }

} 