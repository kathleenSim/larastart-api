<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 05:45
 */

namespace app\Domain\Repositories\Eloquent;

use App\Domain\Contracts\RoutesContract;
use App\Domain\Models\Project;

class RoutesRepository implements RoutesContract{


    public function getAll($id=null){

        $mid = func_get_arg(1);

        $rid = func_get_arg(2);

        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->routes()->get()->toArray();


    }

    public function getPaginated(){

    }

    public function getByKey($id){

        $mid = func_get_arg(1);

        $rid = func_get_arg(2);

        $roid = func_get_arg(3);

        return  Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->routes()->where('id', $roid)->first()->toArray();


    }

    public function create($id){

        $input = func_get_arg(1);

        $mid = func_get_arg(2);

        $rid = func_get_arg(3);

        $route = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->routes()->create($input);

        $route->deleted_at = null;

        $route->save();

        return $route;

    }

    public function update($id, $input){

        $mid = func_get_arg(2);

        $rid = func_get_arg(3);

        $roid = func_get_arg(4);

        $route = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $rid)->first()->routes()->where('id', $roid)->first();

        foreach ($input as $attr => $value) {

            $route->$attr = $value;
        }

        $route->save();

        return $route;


    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){

    }

} 