<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 28/02/2015
 * Time: 08:11
 */

namespace Larastart\Domain\Repositories\Eloquent;


use Larastart\Domain\Contracts\RequestsContract;
use Larastart\Domain\Models\Project;

class RequestsRepository implements RequestsContract{

    public function getAll($id=null){
        $mid = func_get_arg(1);

        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->get()->toArray();

    }

    public function getPaginated(){

    }

    public function getByKey($id){
        $mid = func_get_arg(1);

        $eid = func_get_arg(2);

        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->where('id', $eid)->get()->toArray();

    }

    public function create($id){
        $input = func_get_arg(1);

        $mid = func_get_arg(2);

        $request = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->create($input);

        $request->deleted_at = null;

        $request->save();

        return $request;

    }

    public function update($id, $input){
        $mid = func_get_arg(2);

        $rid = func_get_arg(3);

        $requests = Project::findOrFail($id)->modules()->where('id', $mid)->first()->requests()->find($rid);
        dd($requests);
        foreach ($input as $attr => $value) {

            $requests->$attr = $value;
        }

        $requests->save();

        return  $requests;

    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){

    }

} 