<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 02/03/2015
 * Time: 19:00
 */

namespace Larastart\Domain\Repositories\Eloquent;

use Larastart\Domain\Contracts\ModelsContract;
use Larastart\Domain\Models\Project;

class ModelsRepository implements ModelsContract{
    public function getAll($id=null){
        $mid = func_get_arg(1);

        $eid = func_get_arg(2);
       // dd(Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->models());
        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->models()->get()->toArray();
    }

    public function getPaginated(){

    }

    public function getByKey($id){
        $mid = func_get_arg(1);

        $eid = func_get_arg(2);

        $moid = func_get_arg(3);

        return  Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->models()->where('id', $moid)->first()->toArray();

    }

    public function create($id){
        $input = func_get_arg(1);

        $mid = func_get_arg(2);

        $eid = func_get_arg(3);

        $model = Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->models()->create($input);

        $model->deleted_at = null;

        $model->save();

        return $model;
    }

    public function update($id, $input){

        $mid = func_get_arg(2);

        $eid = func_get_arg(3);

        $moid = func_get_arg(4);

        $model = Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->models()->where('id', $moid)->first();

        foreach ($input as $attr => $value) {

            $model->$attr = $value;
        }

        $model->save();

        return $model;


    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){

    }


} 