<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 02/03/2015
 * Time: 19:00
 */

namespace Larastart\Domain\Repositories\Eloquent;


use Larastart\Domain\Contracts\SeedsContract;
use Larastart\Domain\Models\Project;

class SeedsRepository implements SeedsContract{

    function query($id, $mid, $eid)
    {
        return Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->seeds();
    }

    public function getAll($id=null){
        $mid = func_get_arg(1);

        $eid = func_get_arg(2);

        return $this->query($id,$mid,$eid)->get()->toArray();
    }

    public function getPaginated(){

    }

    public function getByKey($id){
        $mid = func_get_arg(1);

        $eid = func_get_arg(2);

        $sid = func_get_arg(3);

        return $this->query($id,$mid,$eid)->where('id', $sid)->first()->toArray();

    }


    public function create($id){
        $input = func_get_arg(1);

        $mid = func_get_arg(2);

        $eid = func_get_arg(3);

      $seed =   Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->seeds()->create($input);

        $seed->deleted_at = null;

        $seed->save();

        return $seed;

    }

    public function update($id, $input){

        $mid = func_get_arg(2);

        $eid = func_get_arg(3);

        $sid = func_get_arg(4);

        $seed = Project::findOrFail($id)->modules()->where('id', $mid)->first()->entities()->where('id', $eid)->first()->seeds()->where('id', $sid)->first();

        foreach ($input as $attr => $value) {

            $seed->$attr = $value;
        }

        $seed->save();

        return $seed;


    }

    public function archive($id){

    }

    public function restore($id){

    }

    public function delete($id){

    }


} 