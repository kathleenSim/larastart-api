<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 02/03/2015
 * Time: 19:09
 */

namespace Larastart\Http\Controllers;
use Larastart\Domain\Repositories\Eloquent\ModelsRepository as Model;


class ModelsController extends Controller{
    function __construct(Model $model)
    {

        $this->model = $model;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id, $mid, $eid)
    {
        $models = $this->model->getAll($id, $mid, $eid);

        return response($models, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid, $eid, $moid)
    {
        $model = $this->model->getByKey($id, $mid, $eid, $moid);

        return response($model, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, $mid, $eid)
    {
        $model = $this->model->create($id, \Input::all(), $mid, $eid);

        return $model;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid, $eid, $moid)
    {
        $input = array_filter(\Input::all());

        $model = $this->model->update($id, $input, $mid, $eid, $moid);

        return response($model, 200);
    }
} 