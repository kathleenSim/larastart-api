<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 03:16
 */

namespace App\Http\Controllers;
use App\Domain\Repositories\Eloquent\ControllersRepository as ControllerZ;


class ControllersController extends Controller{

    function __construct(ControllerZ $controller)
    {

        $this->controller = $controller;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id, $mid, $rid)
    {
        $controllers = $this->controller->getAll($id, $mid, $rid);

        return response($controllers, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid, $rid, $cid)
    {
        $controller = $this->controller->getByKey($id, $mid, $rid, $cid);

        return response($controller, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, $mid, $rid)
    {
        $controller = $this->controller->create($id, \Input::all(), $mid, $rid);

        return $controller;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid, $rid, $cid)
    {
        $input = array_filter(\Input::all());

        $controller = $this->controller->update($id, $input, $mid, $rid, $cid);

        return response($controller, 200);
    }

} 