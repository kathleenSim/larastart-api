<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 25/03/2015
 * Time: 04:35
 */

namespace Larastart\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Larastart\Domain\Models\User;
use Session;
use Larastart\Http\Controllers\Controller;

class AuthenticationController extends Controller{

    function authorise()
    {
        // get the data from the check-authorization-params filter
        $params = Session::get('authorize-params');

        // get the user id
        $params['user_id'] = Auth::user()->id;

        if (Input::has('_token')) {
            // check if the user approved or denied the authorization request
            if (Input::get('approve') !== null) {

                $code = AuthorizationServer::newAuthorizeRequest('user', $params['user_id'], $params);

                Session::forget('authorize-params');
                return Redirect::to(AuthorizationServer::makeRedirectWithCode($code, $params));
            }

            if (Input::get('deny') !== null) {

                Session::forget('authorize-params');

                return Redirect::to(AuthorizationServer::makeRedirectWithError($params));
            }
        }
        // display the authorization form
        return View::make('authorization-form', array('params' => $params));
    }

    function getLogin(){
        if(\Input::has('email'))
        {
            \Auth::attempt(\Input::only('email','password'));
            return \Redirect::intended('/');
        }
        return \View::make('auth.login');
    }

    function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $auth = \Auth::attempt(array(
            'username' => $request['username'],
            'password' => $request['password']
        ));

        if($auth){
            return 'Successfully logged in!';
        }else{
            return 'Login failed.';
        }

    }



    function register(Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'email' => 'required|unique:email',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'username' => 'required|unique:username',
            'password' => 'required|min:6',
            'rpassword' => 'required|same:password'
        ]);

        $fullname = $request['fullname'];
        $email = $request['email'];
        $address = $request['address'];
        $city = $request['city'];
        $country = $request['country'];
        $username = $request['username'];
        $password = $password = \Hash::make($request['password']);

        $user = User::create(array(
                'fullname' => $fullname,
                'email' => $email,
                'address' => $address,
                'city' => $city,
                'country' => $country,
                'username' => $username,
                'password' => $password
            ));
        if($user){
            return 'successfully registered!';
        }

    }

    function getToken()
    {
        if(\Input::get('grant_type') == 'authorization_code')
            $this->beforeFilter('auth');
        return \Authorizer::issueAccessToken();
    }


    function resetPassword()
    {

    }

    function recoverPassword()
    {

    }


    function logout()
    {

    }

} 