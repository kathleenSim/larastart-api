<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 05:54
 */

namespace App\Http\Controllers;

use App\Domain\Repositories\Eloquent\RoutesRepository as Route;

class RoutesController extends Controller{
    function __construct(Route $route)
    {

        $this->route = $route;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id, $mid, $rid)
    {
        $routes = $this->route->getAll($id, $mid, $rid);

        return response($routes, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid, $rid, $roid)
    {
        $route = $this->route->getByKey($id, $mid, $rid, $roid);

        return response($route, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, $mid, $rid)
    {
        $route = $this->route->create($id, \Input::all(), $mid, $rid);

        return $route;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid, $rid, $roid)
    {
        $input = array_filter(\Input::all());

        $route = $this->route->update($id, $input, $mid, $rid, $roid);

        return response($route, 200);
    }


} 