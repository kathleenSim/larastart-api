<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 02/03/2015
 * Time: 19:09
 */

namespace Larastart\Http\Controllers;
use Larastart\Domain\Repositories\Eloquent\TablesRepository as Table;


class TablesController extends Controller{

    function __construct(Table $table)
    {

        $this->table = $table;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id, $mid, $eid)
    {
        $tables = $this->table->getAll($id, $mid, $eid);

        return response($tables, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid, $eid, $tid)
    {
        $table = $this->table->getByKey($id, $mid, $eid, $tid);

        return response($table, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, $mid, $eid)
    {
        $table = $this->table->create($id, \Input::all(), $mid, $eid);

        return $table;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid, $eid, $tid)
    {
        $input = array_filter(\Input::all());

        $table = $this->table->update($id, $input, $mid, $eid, $tid);

        return response($table, 200);
    }

} 