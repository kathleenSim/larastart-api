<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 02/03/2015
 * Time: 19:09
 */

namespace Larastart\Http\Controllers;
use Larastart\Domain\Repositories\Eloquent\SeedsRepository as Seed;


class SeedsController extends Controller{
    function __construct(Seed $seed)
    {

        $this->seed = $seed;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id, $mid, $eid)
    {
        $seeds = $this->seed->getAll($id, $mid, $eid);

        return response($seeds, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid, $eid, $sid)
    {
        $seed = $this->seed->getByKey($id, $mid, $eid, $sid);

        return response($seed, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, $mid, $eid)
    {
        $seed = $this->seed->create($id, \Input::all(), $mid, $eid);

        return $seed;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid, $eid, $sid)
    {
        $input = array_filter(\Input::all());

        $seed = $this->seed->update($id, $input, $mid, $eid, $sid);

        return response($seed, 200);
    }
} 