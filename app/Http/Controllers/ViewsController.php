<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 03:16
 */

namespace App\Http\Controllers;
use App\Domain\Repositories\Eloquent\ViewsRepository as View;


class ViewsController extends Controller{

    function __construct(View $view)
    {

        $this->view = $view;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id, $mid, $rid)
    {
        $views = $this->view->getAll($id, $mid, $rid);

        return response($views, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid, $rid, $vid)
    {
        $view = $this->view->getByKey($id, $mid, $rid, $vid);

        return response($view, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, $mid, $rid)
    {
        $view = $this->view->create($id, \Input::all(), $mid, $rid);

        return $view;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid, $rid, $vid)
    {
        $input = array_filter(\Input::all());

        $view = $this->view->update($id, $input, $mid, $rid, $vid);

        return response($view, 200);
    }

} 