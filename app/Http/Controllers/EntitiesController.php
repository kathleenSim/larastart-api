<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 28/02/2015
 * Time: 10:04
 */

namespace Larastart\Http\Controllers;
use Larastart\Domain\Repositories\Eloquent\EntitiesRepository as Entity;


class EntitiesController extends Controller{

    function __construct(Entity $entity)
    {

        $this->entity = $entity;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id, $mid)
    {
        $entities = $this->entity->getAll($id, $mid);

        return response($entities, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid, $eid)
    {
        $entity = $this->entity->getByKey($id, $mid, $eid);

        return response($entity, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, $mid)
    {
        $entity = $this->entity->create($id, \Input::all(), $mid);

        return $entity;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid, $eid)
    {
        $input = array_filter(\Input::all());

        $entity = $this->entity->update($id, $input, $mid, $eid);

        return response($entity, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id, $mid, $eid)
    {
        return $this->entity->delete($id, $mid, $eid);
    }

} 