<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 25/02/2015
 * Time: 15:26
 */

namespace Larastart\Http\Controllers;
use Larastart\Domain\Repositories\Eloquent\PackagesRepository as Package;
use Larastart\Http\Transformers\PackageTransformer;


class PackagesController extends Controller{

    function __construct(PackageTransformer $packageTransformer,Package $package)
    {
        $this->packageTransformer = $packageTransformer;

        $this->package = $package;

    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {
       $packages = $this->package->getAll($id);

        return response($packages, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $pid)
    {
        $package = $this->package->getByKey($id, $pid);

        return response($package, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id)
    {
        $package = $this->package->create($id, \Input::all());

        return $package;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $pid)
    {
        $input = array_filter(\Input::only(['name', 'version', 'options', 'providers', 'aliases']));

        $package = $this->package->update($id, $input, $pid);

        return response($package, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id, $pid)
    {
        return $this->package->delete($id, $pid);
    }

} 