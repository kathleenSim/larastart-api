<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 03:16
 */

namespace App\Http\Controllers;
use App\Domain\Repositories\Eloquent\TransformersRepository as Transformer;


class TransformersController extends Controller{
    function __construct(Transformer $transformer)
    {

        $this->transformer = $transformer;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id, $mid, $rid)
    {
        $transformers = $this->transformer->getAll($id, $mid, $rid);

        return response($transformers, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid, $rid, $tid)
    {
        $transformer = $this->transformer->getByKey($id, $mid, $rid, $tid);

        return response($transformer, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, $mid, $rid)
    {
        $transformer = $this->transformer->create($id, \Input::all(), $mid, $rid);

        return $transformer;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid, $rid, $tid)
    {
        $input = array_filter(\Input::all());

        $transformer = $this->transformer->update($id, $input, $mid, $rid, $tid);

        return response($transformer, 200);
    }

} 