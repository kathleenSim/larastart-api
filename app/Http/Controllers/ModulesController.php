<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 28/02/2015
 * Time: 06:12
 */

namespace Larastart\Http\Controllers;
use Larastart\Domain\Repositories\Eloquent\ModulesRepository as Module;


class ModulesController extends Controller{

    function __construct(Module $module)
    {

        $this->module = $module;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {
        $modules = $this->module->getAll($id);

        return response($modules, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid)
    {
        $module = $this->module->getByKey($id, $mid);

        return response($module, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id)
    {
        $module = $this->module->create($id, \Input::all());

        return $module;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid)
    {
        $input = array_filter(\Input::all());

        $module = $this->module->update($id, $input, $mid);

        return response($module, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id, $mid)
    {
        return $this->module->delete($id, $mid);
    }

} 