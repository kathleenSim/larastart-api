<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 17/02/2015
 * Time: 03:23
 */

namespace Larastart\Http\Controllers;

use Larastart\Commands\BuildProjectCommand;
use Validator;
use Input;
use Larastart\Http\Transformers\ProjectTransformer;
use Larastart\Domain\Repositories\Eloquent\ProjectsRepository as Project;
use Larastart\Http\Requests\ProjectFormRequest;


class ProjectsController extends Controller
{

    protected $projectTransformer;

    function __construct(ProjectTransformer $projectTransformer, Project $project)
    {

        $this->projectTransformer = $projectTransformer;

        $this->project = $project;

    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $projects = $this->project->getAll();

        $projects = $this->projectTransformer->transformCollection($projects);

        return response($projects, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
            $project = $this->project->getByKey($id);

            $project = $this->projectTransformer->transform($project);

            return response($project, 200);
        }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ProjectFormRequest $request)
    {

            $project = $this->project->create($request);

            $project = $this->projectTransformer->transform($project);

            return response($project, 201);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_filter(Input::only(['name', 'version', 'slug']));

        $project = $this->project->update($id, $input);

        $project = $this->projectTransformer->transform($project);

        return response($project, 200);
    }

    /**
     * Softdelete the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function archive($id)
    {

        return $this->project->archive($id);

    }

    /**
     * Restore archived/softdeleted resource
     *
     * @param  int $id
     * @return Response
     */
    public function restore($id)
    {

        $project = $this->project->restore($id);

        $project = $this->projectTransformer->transform($project);

        return response($project, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {

        return $this->project->delete($id);

    }


    public function build($id)
    {
        $this->dispatch(new BuildProjectCommand($this->project->getByKey($id)));
        return response([
            'message' => 'Build has been successfully scheduled'
        ],203);
    }

}