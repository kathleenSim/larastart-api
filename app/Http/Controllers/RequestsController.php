<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 28/02/2015
 * Time: 10:05
 */

namespace Larastart\Http\Controllers;
use Larastart\Domain\Repositories\Eloquent\RequestsRepository as Request;


class RequestsController extends Controller{

    function __construct(Request $request)
    {

        $this->request = $request;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id, $mid)
    {
        $requests = $this->request->getAll($id, $mid);

        return response($requests, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $mid, $rid)
    {
        $requests = $this->request->getByKey($id, $mid, $rid);

        return response($requests, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, $mid)
    {
        $requests = $this->request->create($id, \Input::all(), $mid);

        return $requests;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, $mid, $rid)
    {
        $input = array_filter(\Input::all());

        $requests = $this->request->update($id, $input, $mid, $rid);

        return response($requests, 200);
    }
} 