<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('auth/login', array('before' => 'guest', 'uses' => 'Auth\AuthenticationController@getLogin'));

Route::post('auth/login', array('before' => 'guest|csrf', 'uses' => 'Auth\AuthenticationController@postLogin'));

Route::post('auth/register', array('before' => 'guest', 'uses' => 'Auth\AuthenticationController@register'));

Route::get('auth/recover', array('before' => 'guest', 'uses' => 'Auth\AuthenticationController@recoverPassword'));

Route::get('auth/reset', array('before' => 'guest', 'uses' => 'Auth\PasswordController'));

Route::post('auth/reset', array('before' => 'guest', 'uses' => 'Auth\PasswordController'));

Route::get('token', array('uses' => 'Auth\AuthenticationController@getToken'));

Route::group(['prefix' => 'api/v1'], function () {

        Route::resource('projects', 'ProjectsController');

        Route::put('projects/{projects}/restore', 'ProjectsController@restore');

        Route::delete('projects/{projects}/archive', 'ProjectsController@archive');

        Route::get('projects/{projects}/build', 'ProjectsController@build');

        Route::resource('projects.packages', 'PackagesController');

        Route::resource('projects.modules', 'ModulesController');

        Route::resource('projects.modules.entities', 'EntitiesController');

        Route::resource('projects.modules.requests', 'RequestsController');

        Route::resource('projects.modules.requests.controllers', 'ControllersController');

        Route::resource('projects.modules.requests.views', 'ViewsController');

        Route::resource('projects.modules.requests.routes', 'RoutesController');

        Route::resource('projects.modules.requests.transformers', 'TransformersController');

        Route::resource('projects.modules.entities.tables', 'TablesController');

        Route::resource('projects.modules.entities.seeds', 'SeedsController');

        Route::resource('projects.modules.entities.models', 'ModelsController');


});