<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 25/02/2015
 * Time: 14:28
 */

namespace Larastart\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ProjectFormRequest extends FormRequest {

    public function rules()
    {
        return [
            'name' => 'required'
//            'version' => 'required',
//            'type' => 'required',
//            'status' => 'required'
        ];
    }

    public function response(array $errors)
    {
        return response(["message" => "Validation failed!",
                "errors" => $errors
            ], 422);
    }

    public function authorize()
    {

        return true;
    }
} 