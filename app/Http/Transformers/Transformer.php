<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 20/02/2015
 * Time: 07:35
 */

namespace Larastart\Http\Transformers;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;


abstract class Transformer {

    public function transformArray(array $items){

        return array_map([$this, 'transform'], $items);

    }

    public function transformCollection(Collection $collection){

        return $this->transformArray($collection->toArray());

    }

    public abstract function transform($item);

    public function transformPaginator(LengthAwarePaginator $paginator){

        return $this->transformArray($paginator->toArray()['data']);

    }

} 