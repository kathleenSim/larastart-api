<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 19/02/2015
 * Time: 05:47
 */

namespace Larastart\Http\Transformers;

class ProjectTransformer extends Transformer{

    public function transform($project){

        return [

            'id' => $project['_id'],
            'name' => $project['name'],
            'slug' => $project['slug'],
            'version' => $project['version'],
            'status' => $project['status'],
            'type' => $project['type'],
            'created_at' => $project['created_at'] ,
            'last_updated' => $project['updated_at'],
            'archived_at' => $project['deleted_at']

        ];

    }

} 