<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 13/03/2015
 * Time: 15:19
 */

namespace Larastart\Http\Transformers;


class PackageTransformer extends Transformer{

    public function transform($project){

        return [

            'id' => $project['_id'],
            'name' => $project['name'],
            'version' => $project['version'],
            'options' => $project['options'],
            'providers' => $project['providers'] ,
            'aliases' => $project['aliases'],
            'created_at' => $project['created_at'] ,
            'last_updated' => $project['updated_at'],

        ];

    }
} 