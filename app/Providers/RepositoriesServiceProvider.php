<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 17/02/2015
 * Time: 04:14
 */

namespace Larastart\Providers;


use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider{

    public function register()
    {
        $this->app->bind(
            'Larastart\Domain\Contracts\ProjectsContract',
            'Larastart\Domain\Repositories\ProjectsRepository'
        );

        $this->app->bind(
            'Larastart\Domain\Contracts\PackagesContract',
            'Larastart\Domain\Repositories\PackagesRepository'
        );

        $this->app->bind(
            'Larastart\Domain\Contracts\ModulesContract',
            'Larastart\Domain\Repositories\ModulesRepository'
        );

        $this->app->bind(
            'Larastart\Domain\Contracts\EntitiesContract',
            'Larastart\Domain\Repositories\EntitiesRepository'
        );

        $this->app->bind(
            'Larastart\Domain\Contracts\RequestsContract',
            'Larastart\Domain\Repositories\RequestsRepository'
        );

        $this->app->bind(
            'Larastart\Domain\Contracts\SeedsContract',
            'Larastart\Domain\Repositories\SeedsRepository'
        );

        $this->app->bind(
            'Larastart\Domain\Contracts\TablesContract',
            'Larastart\Domain\Repositories\TablesRepository'
        );

        $this->app->bind(
            'Larastart\Domain\Contracts\ModelsContract',
            'Larastart\Domain\Repositories\ModelsRepository'
        );

        $this->app->bind(
            'App\Domain\Contracts\ControllersContract',
            'App\Domain\Repositories\ControllersRepository'
        );

        $this->app->bind(
            'App\Domain\Contracts\TransformersContract',
            'App\Domain\Repositories\TransformersRepository'
        );

        $this->app->bind(
            'App\Domain\Contracts\ViewsContract',
            'App\Domain\Repositories\ViewsRepository'
        );

        $this->app->bind(
            'App\Domain\Contracts\RoutesContract',
            'App\Domain\Repositories\RoutesRepository'
        );
    }

} 