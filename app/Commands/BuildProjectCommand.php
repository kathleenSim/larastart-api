<?php namespace Larastart\Commands;

use app\Exceptions\InvalidVersionException;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Bus\SelfHandling;
use Larastart\Domain\Models\Project;
use Larastart\Events\Build\BaseAppCreating;
use Larastart\Events\Build\BaseAppCreated;
use Larastart\Events\Build\BuildCompleted;
use Larastart\Events\Build\BuildStarting;
use Larastart\Events\Build\ComposerUpdated;
use Larastart\Events\Build\ComposerUpdating;
use Larastart\Events\Build\FilesGenerated;
use Larastart\Events\Build\FilesGenerating;

/**
 * @property Project project
 */
class BuildProjectCommand extends Command implements SelfHandling, ShouldBeQueued
{
    use SerializesModels;

    protected $project;

    /**
     * Create a new command instance.
     *
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Build the project by firing a series of commands and have listeners build the app
     *
     * @return void
     */
    public function handle()
    {
        #Initialize the build process
        event(new BuildStarting($this->project));

        #create the base app
        event(new BaseAppCreating($this->project));
//        $this->createBaseApp();
        event(new BaseAppCreated($this->project));

        #Generate the necessary files
        event(new FilesGenerating($this->project));
        $this->generateFiles();
        event(new FilesGenerated($this->project));

        #Run composer update
        event(new ComposerUpdating($this->project));
        $this->runComposerUpdate();
        event(new ComposerUpdated($this->project));

        #Fire build completed event
        event(new BuildCompleted($this->project));
    }


    public function createBaseApp()
    {
        //TODO: Log start
        $source = base_path('resources/stubs/laravel/' . $this->project->version);
        if(!\File::exists($source))
        {
            throw new InvalidVersionException($this->project->version);
        }

        if(!\File::exists($this->project->basePath))
        {
            mkdir($this->project->basePath,0644,true);
        }

        if(\File::exists($this->project->buildPath.'/current'))
        {
            $current = $this->project->buildPath.'/current';
            $timestamp = \File::lastModified($current);
            \File::move($current, $this->project->buildPath.'/'.$timestamp);
        }

        return passthru('cp -r '.$source.' '.$this->project->buildPath.'/current');
        //TODO: Log complete
    }

    public function generateFiles()
    {
    }

    public function runComposerUpdate()
    {

    }


}
