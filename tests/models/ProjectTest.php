<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 05/03/2015
 * Time: 04:21
 */

namespace tests\models;

use App\Domain\Repositories\Eloquent\ProjectsRepository;
use App\Domain\Models\Project;

class ProjectTest extends \TestCase{

//    /** @test */
//    public function it_fetches_projects(){
//
//        $project = new Project("Project A");
//    }

    /** @test */
    public function it_fetches_a_single_project()
    {
        //$id = 1;
        $project = $this->make();

        $result = $this->getJson('api/v1/projects/'. $project[0]->id);

        $this->assertEquals("Project A", $result);
    }

    public function getJson($uri, $method = 'GET', $parameters = [] ){

        return json_decode($this->call($method, $uri, $parameters)->getContent());

    }

    protected function make(){
        return [
           'name' => 'Project A'
        ] ;
    }
} 