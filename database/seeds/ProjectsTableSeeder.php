<?php
/**
 * Created by PhpStorm.
 * User: Kathleen
 * Date: 17/02/2015
 * Time: 09:51
 */

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProjectsTableSeeder extends Seeder{



        public function run()
        {
            DB::collection('projects')->delete();

            $faker = Faker::create();

            foreach (range(1, 20) as $index) {
                $projects = [
                    'owner_id'  => $faker->randomDigit(5),
                    'name'      => 'trial 1',
                    'version'   => '2.0',
                    'type'      => 'API'
                ];
                DB::table('projects')->insert($this->createProject());
            }
        }


    function createProject()
    {
        $faker = Faker::create();

        return $projects = [
            'created_at'        =>  new MongoDate(),
            'updated_at'        =>  new MongoDate(),
            'deleted_at'        =>  null,
            'owner_id'          => $faker->randomDigit(5),
            'name'              =>  $faker->word,
            'slug'              =>  rand(1, 99999),
            'version'           =>  '5.0',
            'type'              =>  'API',
            'status'            =>  'new',
            'packages'          => $this->createPackages(1),
            'modules'           => $this->createModules(1)
        ];
    }

    function createPackages($quantity  = 1)
    {
        $faker = Faker::create();

        $packages = [];
        while($quantity > 0)
        {
            $packages[] = [
                    '_id'                =>  $faker->uuid,
                    'created_at'        =>  new MongoDate(),
                    'updated_at'        =>  new MongoDate(),
                    'name'      => $faker->word,
                    'version'   => '',
                    'options'   => [],
                    'providers' => '',
                    'aliases'   => $this->createAlias(1)
            ];
            $quantity--;
        }
        return $packages;
    }

    function createAlias($quantity = 1)
    {
        $faker = Faker::create();

        $alias = [];
        while($quantity > 0)
        {
            $alias[] = [
                'alias' => $faker->word,
                'facade'=> $faker->word
            ];
            $quantity--;
        }
        return $alias;
    }

    function createModules($quantity = 1)
    {
        $faker = Faker::create();

        $modules = [];
        while($quantity > 0)
        {
            $modules[] = [
                '_id'                =>  $faker->uuid,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'name'              =>  $faker->word,
                'entities'          =>  $this->createEntities(1),
                'requests'            => $this->createModuleRequests(1)
            ];
            $quantity--;
        }
        return $modules;
    }

    function createEntities($quantity = 1)
    {
        $faker = Faker::create();

        $entities = [];
        while($quantity > 0)
        {
            $entities[] = [
                '_id'                =>  $faker->uuid,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'name'   =>  $faker->word,
                'tables' =>  $this->createEntitiesTables(1),
                'seeds'  =>  $this->createEntitySeeds(1),
                'models' =>  $this->createEntityModels(1)
            ];
            $quantity--;
        }
        return $entities;
    }

    function createEntitiesTables($quantity = 1)
    {
        $faker = Faker::create();

        $tables = [];
        while($quantity > 0)
        {
            $tables[] = [
                '_id'                =>  $faker->uuid,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'name'    =>$faker->word,
                'type'    =>'',
                'nullable'      =>'',
                'primary'       =>'',
                'unique'        =>'',
                'index'         =>''
            ];
            $quantity--;
        }
        return $tables;
    }

    function createEntitySeeds($quantity = 1)
    {
        $faker = Faker::create();

        $seeds = [];
        while($quantity > 0)
        {
            $seeds[] = [
                '_id'                =>  $faker->uuid,
                'name'              =>  $faker->word,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'field'     =>'',
                'seeds'     =>'',
                'sample'    =>''
            ];
            $quantity--;
        }
        return $seeds;
    }

    function createEntityModels($quantity = 1)
    {
        $faker = Faker::create();

        $models = [];
        while($quantity > 0)
        {
            $models[] = [
                '_id'                =>  $faker->uuid,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'name'        =>  $faker->word,
                'attributes'        =>  '',
                'relationship_name' =>  '',
                'relationship_type' =>  '',
                'related'           =>  '',
                'extras'            =>  ''
            ];
            $quantity--;
        }
        return $models;
    }

    function createModuleRequests($quantity = 1)
    {
        $faker = Faker::create();

        $requests = [];
        while($quantity > 0)
        {
            $requests[] = [
                '_id'                =>  $faker->uuid,
                'name'              => $faker->word,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'routes'            => $this->createRequestsRoutes(1),
                'controllers'=>      $this->createRequestsControllers(1),
                'views'      =>      $this->createRequestsViews(1),
                'transformers'=>     $this->createRequestsTransformers(1)
            ];
            $quantity--;
        }
        return $requests;
    }

    function createRequestsControllers($quantity = 1)
    {
        $faker = Faker::create();

        $controllers = [];
        while($quantity > 0)
        {
            $controllers[] = [
                '_id'                =>  $faker->uuid,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'name'   =>  $faker->word,
                'data'   =>  ''
            ];
            $quantity--;
        }
        return $controllers;
    }

    function createRequestsViews($quantity = 1)
    {
        $faker = Faker::create();

        $views = [];
        while($quantity > 0)
        {
            $views[] = [
                '_id'                =>  $faker->uuid,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'name'     =>  $faker->word,
                'path'     =>  '',
                'type'     =>  '',
                'fields'   =>  ''
            ];
            $quantity--;
        }
        return $views;
    }

    function createRequestsTransformers($quantity = 1)
    {
        $faker = Faker::create();

        $transformers = [];
        while($quantity > 0)
        {
            $transformers[] = [
                '_id'                =>  $faker->uuid,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'name'      =>  $faker->word,
                'mapping'   =>  ''
            ];
            $quantity--;
        }
        return $transformers;
    }

    function createRequestsRoutes($quantity = 1)
    {
        $faker = Faker::create();

        $routes = [];
        while($quantity > 0)
        {
            $routes[] = [
                '_id'                =>  $faker->uuid,
                'name'              => $faker->word,
                'created_at'        =>  new MongoDate(),
                'updated_at'        =>  new MongoDate(),
                'domain'     =>      '',
                'before_filter'     =>      '',
                'after_filter'      =>      '',
                'action'            =>      '',
            ];
            $quantity--;
        }
        return $routes;
    }
} 