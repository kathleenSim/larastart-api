<?php

use Illuminate\Database\Seeder;

class OAuthSeeder extends Seeder
{
    function run()
    {
        $this->addClients();
        $this->addScopes();
        $this->addGrantTypes();
        $this->linkClientsToGrants();
        $this->linkClientsToScopes();
        $this->linkGrantsToScopes();
    }

    protected function addClients()
    {
        DB::collection('oauth_clients')->delete();
        DB::collection('oauth_clients')->insert(
            [
                'id' => 'special-key',
                'name' => 'Api Docs Key',
                'secret' => 'special-secret',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );
        $this->command->info('OAuth Clients Added');
    }

    private function addScopes()
    {
        DB::collection('oauth_scopes')->delete();
        DB::collection('oauth_scopes')->insert([
            'name' => 'user',
            'scope' => 'user',
            'description' => 'user scope',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('oauth_scopes')->insert([
            'name' => 'admin',
            'scope' => 'admin',
            'description' => 'admin scope',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        $this->command->info('OAuth Scopes Added');
    }

    function addGrantTypes()
    {
        $grants = ['client_credentials', 'authorization_code', 'password',];
        DB::collection('oauth_grants')->delete();
        foreach ($grants as $grant)
        {
            DB::collection('oauth_grants')->insert([
                'grant' => $grant,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        $this->command->info('OAuth Grants Added');
    }

    function linkClientsToGrants()
    {

        DB::collection('oauth_client_grants')->delete();
        $client = DB::collection('oauth_clients')->where('id','special-key')->pluck('id');
        $grants = DB::collection('oauth_grants')->lists('id');
        foreach ($grants as $grant)
        {
            DB::collection('oauth_client_grants')->insert([
                'client_id' => $client,
                'grant_id' => $grant,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        $this->command->info('OAuth Clients Linked to Grants');
    }

    function linkClientsToScopes()
    {

        DB::collection('oauth_client_scopes')->delete();
        $client = DB::collection('oauth_clients')->where('id','special-key')->pluck('id');
        $scopes = DB::collection('oauth_scopes')->lists('id');
        foreach ($scopes as $scope)
        {
            DB::collection('oauth_client_scopes')->insert([
                'client_id' => $client,
                'scope_id' => $scope,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        $this->command->info('OAuth Clients Linked to Scopes');
    }

    function linkGrantsToScopes()
    {
        DB::collection('oauth_grant_scopes')->delete();
        $user_scope = DB::collection('oauth_scopes')->where('scope','user')->pluck('id');
        $admin_scope = DB::collection('oauth_scopes')->where('scope','admin')->pluck('id');

        $admin_grants = DB::collection('oauth_grants')->where('grant','client_credetials')->lists('id');
        $user_grants = DB::collection('oauth_grants')->where('grant','!=' ,'client_credetials')->lists('id');

        foreach($user_grants as $grant)
        {
            DB::collection('oauth_grant_scopes')->insert([
                'scope_id' => $user_scope,
                'grant_id' => $grant,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        foreach($admin_grants as $grant)
        {
            DB::collection('oauth_grant_scopes')->insert([
                'scope_id' => $admin_scope,
                'grant_id' => $grant,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
        $this->command->info('OAuth Scopes Linked to Grants');
    }

}